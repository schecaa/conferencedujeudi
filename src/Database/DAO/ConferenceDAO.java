/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import Database.DatabaseUtilies;
import static Database.DatabaseUtilies.getConnexion;
import Database.POJO.Conference;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Segolène
 */
public class ConferenceDAO {
 
    public static ArrayList<Conference> getAll(){
        ArrayList<Conference> liste = new ArrayList<>(); //Création de la liste conference
        Connection con = getConnexion(); // connexion
        String query = "SELECT * FROM conference"; //requete
        ResultSet res = null;
        Statement stmt =null;        // déclaration d'un objet statement (sert a faire la relation entre connexion et la base de donné
        try {
            stmt=con.createStatement() ;
            res = stmt.executeQuery(query);
            while (res.next()){
                //on recupere les valeurs dans chaque colone
                int idConference = res.getInt("idConference");
                String titre = res.getString("titreConference");
                Date date = res.getDate("dateConference");
                //créer un objet de type calendar
                Calendar cal = Calendar.getInstance();
                //on veut attribue la valeur date à cal ; TimeInMillis = milli seconde
                cal.setTimeInMillis(date.getTime());
                int idSalle = res.getInt("idSalle");
                int idTheme = res.getInt("idTheme");
                //on créer l'objet conference à l'aide des valeurs
                Conference conference1 = new Conference(titre, cal, idConference, idSalle, idTheme);
                liste.add(conference1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return liste;
    }
    
    public static Conference get(int id) {
        Conference conference1=null;
        try {
            PreparedStatement ps = null;
            Connection con = getConnexion();
            //creer requete
            String query = "SELECT * FROM `conference` WHERE idConference = ?";
            // récuperrer le ResultSet
            ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ResultSet res = ps.executeQuery();
            //parcourir le resultSet et récupérer tous les attributs
            while (res.next()){
                //on recupere les valeurs dans chaque colone
                int idConference = res.getInt("idConference");
                String titre = res.getString("titreConference");
                Date date = res.getDate("dateConference");
                //créer un objet de type calendar
                Calendar cal = Calendar.getInstance();
                //on veut attribue la valeur date à cal ; TimeInMillis = milli seconde
                cal.setTimeInMillis(date.getTime());
                int idSalle = res.getInt("idSalle");
                int idTheme = res.getInt("idTheme");
                //on créer l'objet conference à l'aide des valeurs
                conference1 = new Conference(titre, cal, idConference, idSalle, idTheme);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            //on retourne l'objet conference crée
            return conference1;
    }
    
    public static void delete(int id){
        try {
            Connection con = getConnexion(); // connexion
            PreparedStatement ps = null;
            String query = "DELETE FROM conference WHERE idConference = ?";
            // récuperrer le ResultSet
            ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ps.execute(); //on retourne un boolean
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void insert(Conference c){
        Connection con = getConnexion();
        PreparedStatement ps = null;
        String query ="INSERT INTO conference('titreConference', 'dateConference', 'idConferencier', 'idSalle', 'idTheme') VALUES (?, ?, ?, ?, ?)";
        try {
            ps = con.prepareStatement(query);
            ps.setString(1, c.getTitreConference());
            ps.setDate(2, c.getSqlDate());
            ps.setInt(3, c.getIdConferencier());
            ps.setInt(4, c.getIdSalle());
            ps.setInt(5, c.getIdTheme());
            ps.execute();
        }catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void update(Conference c){
        Connection con = getConnexion();
        PreparedStatement ps = null;
        String query ="UPDATE conference SET titreConference= ?, dateConference=?, idConferencier=?, idSalle=?, idTheme=? WHERE titreConference= ?";
        try {
            ps = con.prepareStatement(query);
            ps.setString(1, c.getTitreConference());
            ps.setDate(2, c.getSqlDate());
            ps.setInt(3, c.getIdConferencier());
            ps.setInt(4, c.getIdSalle());
            ps.setInt(5, c.getIdTheme());
            ps.execute();
        }catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
