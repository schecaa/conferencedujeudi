/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilies.getConnexion;
import Database.POJO.ConferenceStatut;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Segolène
 */
public class ConferenceStatutDAO {
    public static ArrayList<ConferenceStatut> getAll(){
        ArrayList<ConferenceStatut> liste = new ArrayList<>(); //Création de la liste conference
        Connection con = getConnexion(); // connexion
        String query = "SELECT * FROM conferencestatut"; //requete
        ResultSet res = null;
        Statement stmt =null;        // déclaration d'un objet statement (sert a faire la relation entre connexion et la base de donné
        try {
            stmt = con.createStatement() ;
            res = stmt.executeQuery(query);
            while (res.next()){
                //on recupere les valeurs dans chaque colone
                int conferenceIdentifiant = res.getInt ("conferenceIdentifiant");
                int statutIdentifian = res.getInt ("StatutIdentifian");
                Date dateStatutConference = res.getDate ("dateStatutConference");
                //créer un objet de type calendar
                Calendar cal = Calendar.getInstance();
                //on veut attribue la valeur date à cal ; TimeInMillis = milli seconde
                cal.setTimeInMillis(dateStatutConference.getTime());
                //on créer l'objet conference à l'aide des valeurs
                ConferenceStatut conferenceS = new ConferenceStatut(conferenceIdentifiant, statutIdentifian, cal);
                liste.add(conferenceS);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return liste;
    }
    
    public static ConferenceStatut get(int id) {
        ConferenceStatut conferenceS=null;
        try {
            PreparedStatement ps = null;
            Connection con = getConnexion();
            //creer requete
            String query = "SELECT * FROM `conference` WHERE idConference = ?";
            // récuperrer le ResultSet
            ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ResultSet res = ps.executeQuery();
            //parcourir le resultSet et récupérer tous les attributs
            while (res.next()){
                //on recupere les valeurs dans chaque colone
                int conferenceIdentifiant = res.getInt ("conferenceIdentifiant");
                int StatutIdentifian = res.getInt ("StatutIdentifian");
                Date dateStatutConference = res.getDate ("dateStatutConference");
                //créer un objet de type calendar
                Calendar cal = Calendar.getInstance();
                //on veut attribue la valeur date à cal ; TimeInMillis = milli seconde
                cal.setTimeInMillis(dateStatutConference.getTime());
                //on créer l'objet conference à l'aide des valeurs
                conferenceS = new ConferenceStatut(conferenceIdentifiant, StatutIdentifian, cal);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            //on retourne l'objet conference crée
            return conferenceS;
    }
    
    public static void delete(int id){
        try {
            Connection con = getConnexion(); // connexion
            PreparedStatement ps = null;
            String query = "DELETE FROM conferencestatut WHERE conferenceIdentifiant = ?";
            // récuperrer le ResultSet
            ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ps.execute(); //on retourne un boolean
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void insert(ConferenceStatut c){
        Connection con = getConnexion();
        PreparedStatement ps = null;
        String query ="INSERT INTO conferencestatut(conferenceIdentifiant, StatutIdentifian, dateStatutConference) VALUES (?, ?, ?)";
        try {
            ps = con.prepareStatement(query);
            ps.setInt(1, c.getConferenceIdentifiant());
            ps.setInt(2, c.getIdStatut());
            ps.setDate(3, c.getSqlDateStatutConference());
            ps.execute();
        }catch (SQLException ex) {
            Logger.getLogger(ConferenceStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void update(ConferenceStatut c){
        Connection con = getConnexion();
        PreparedStatement ps = null;
        String query ="UPDATE conferencestatut SET conferenceIdentifiant = ?, StatutIdentifian=?, dateStatutConference=? WHERE conferenceIdentifiant=? AND StatutIdentifian=?";
        try {
            ps = con.prepareStatement(query);
            ps.setInt(1, c.getConferenceIdentifiant());
            ps.setInt(2, c.getIdStatut());
            ps.setDate(3, c.getSqlDateStatutConference());
            ps.setInt(4, c.getConferenceIdentifiant());
            ps.setInt(5, c.getIdStatut());
            ps.execute();
        }catch (SQLException ex) {
            Logger.getLogger(ConferenceStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
