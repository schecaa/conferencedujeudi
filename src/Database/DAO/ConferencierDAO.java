/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilies.getConnexion;
import Database.POJO.Conferencier;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Segolène
 */
public class ConferencierDAO {
    public static ArrayList<Conferencier> getAll() throws SQLException{
        ArrayList<Conferencier> liste = new ArrayList<>();
        Connection con = getConnexion();
        String query = "SELECT * FROM conferencier";
        ResultSet res =null;
        Statement stmt = null;
        try{
            stmt = con.createStatement();
            res = stmt.executeQuery(query);
            while(res.next()){
                int identifiantConferencier = res.getInt("IdentifiantConferencier");
                String nomPrenomConferencier = res.getString("nomPrenomConferencier");
                String conferencierInterne = res.getString("conferencierInterne");
                Conferencier conferencier = new Conferencier(identifiantConferencier, nomPrenomConferencier, identifiantConferencier);
            }
        }catch (SQLException ex){
            Logger.getLogger(ConferencierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }

    public static Conferencier get(int id) throws SQLException{
        Conferencier conferencier = null;
        try{
            PreparedStatement ps = null;
            Connection con = getConnexion();
            String query = "SELECT * FROM 'conferencier' WHERE IdentifiantConferencier =?";
            ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ResultSet res = ps.executeQuery();
            while (res.next()){
                int identifiantConferencier = res.getInt("IdentifiantConferencier");
                String nomPrenomConferencier = res.getString("nomPrenomConferencier");
                String conferencierInterne = res.getString("conferencierInterne");
                String conferencierIdentifiant = res.getString ("conderencierIdentifiant");
                String blocNoteConferencer = res.getString ("blocNoteConferencer");
                conferencier = new Conferencier(identifiantConferencier, nomPrenomConferencier, id);
            }
        }catch (SQLException ex) {
            Logger.getLogger(ConferencierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conferencier;
    }
    
    public static void delete(int id){
        try {
            Connection con = getConnexion(); // connexion
            PreparedStatement ps = null;
            String query = "DELETE FROM conferencier WHERE IdentifiantConferencier= ?";
            ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ps.execute();
        }catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void insert(Conferencier c) throws SQLException{
        Connection con = getConnexion();
        PreparedStatement ps = null;
        String query ="INSERT INTO conferencier('nomPrenomConferencier', 'conferencierInterne', 'conferenceIdentifiant', 'blocNoteConferencer' VALUES(?, ?, ?, ?, ?)";
        try {
            ps = con.prepareStatement(query);
            ps.setString(1, c.getNomPrenomConferencier());
            ps.setInt(1, c.getIdConferencier());
            ps.setInt(1, c.getIdConference());
            ps.setString(1, c.getBlocNoteConferecier());
            ps.execute();
        }catch (SQLException ex) {
            Logger.getLogger(ConferencierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void update(Conferencier c){
        Connection con = getConnexion();
        PreparedStatement ps = null;
        String query = "UPDATE conferencier SET nomPrenomConferencier=?, conferencierInterne=?, conferenceIdentifiant=?, blocNoteConferencer=? WHERE nomPrenomConferencier=?";
        try {
            ps = con.prepareStatement(query);
            ps.setString(1, c.getBlocNoteConferecier());
            ps.setString(1, c.getNomPrenomConferencier());
            ps.setInt(1, c.getIdConference());
            ps.setInt(1, c.getIdConferencier());
            ps.execute();
        }catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
