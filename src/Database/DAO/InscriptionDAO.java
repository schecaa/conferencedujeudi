/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilies.getConnexion;
import Database.POJO.Conference;
import Database.POJO.Inscription;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Segolène
 */
public class InscriptionDAO {
    public static ArrayList<Inscription> getAll() throws SQLException{
        ArrayList<Inscription> liste = new ArrayList<>();
        Connection con = getConnexion();
        String query = "SELECT * FROM inscription";
        ResultSet res = null;
        Statement stmt = null;
        try {
            stmt=con.createStatement() ;
            res = stmt.executeQuery(query);
            while (res.next()){
                int conferenceIdentification = res.getInt("conferenceIdentification");
                int identificationInscription = res.getInt("identificationInscription");
                int salaireIdentification = res.getInt("salaireIdentification");
                
                Inscription inscription = new Inscription(conferenceIdentification, identificationInscription, salaireIdentification);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InscriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }
    
    public static Inscription get(int id){
        Inscription inscription = null;
        try{
            PreparedStatement ps = null;
            Connection con = getConnexion();
            String query= "SELECT * FROM inscription WHERE identifiantInscription = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ResultSet res = ps.executeQuery();
            while (res.next()){
                int identifiantInscription = res.getInt("identifiantInscription");
                int conferenceIdentification = res.getInt("conferenceIdentification");
                int salarieIdentification = res.getInt("salarieIdentification");
                inscription = new Inscription(identifiantInscription, id, id);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InscriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    return inscription;
    }

    //faire DELETE
    //faire INSERTE
    //faire UPDATE

}
