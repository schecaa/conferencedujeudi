/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Database.POJO.Conference;
import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Segolène
 */
public class DatabaseUtilies {

    public static Connection getConnexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        Connection connection = null;

        try {
            connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/conference", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return connection;
    }

    public static ResultSet exec(String query){
        Connection _c = getConnexion();
        Statement stmt = null;
        ResultSet resultSet = null;
      
        try {
            stmt = _c.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtilies.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            resultSet = stmt.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtilies.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultSet;
    }
}
