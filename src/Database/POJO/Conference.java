/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

import static Database.DatabaseUtilies.getConnexion;
import com.mysql.jdbc.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 *
 * @author Segolène
 */
public class Conference {
    //Déclaration des attributs
    private int idConference;
    private String titreConference;
    private Calendar dateConference;
    private int idConferencier;
    private int idSalle;
    private int idTheme;
    private String nameConferencier;
    private Date sqlDate;
    
    //Création de constructeur
    public Conference(){
        
    }
    
    public Conference(int _idConference, String _titreConference, int _idConferencier){
        this.idConference = _idConference ;
        this.titreConference = _titreConference ;
        this.idConferencier = _idConferencier ;
    }
    
    public Conference(String _titreConference, Calendar _dateConference, int _idConferencier, int _idSalle, int _idTheme){
        this.titreConference = _titreConference ;
        this.idConferencier = _idConferencier ;
        this.dateConference = _dateConference ;
        this.idSalle = _idSalle ;
        this.idTheme = _idTheme;
    }
    
    public Conference(int _idConference, String _titreConference, int _idConferencier, Calendar _dateConference, int _idSalle, int _idTheme, String _nameConferencier, Date _sqlDate){
        this.idConference = _idConference ;
        this.titreConference = _titreConference ;
        this.idConferencier = _idConferencier ;
        this.dateConference = _dateConference ;
        this.idSalle = _idSalle ;
        this.idTheme = _idTheme;
        this.nameConferencier = _nameConferencier;
        this.sqlDate = _sqlDate;
    } 

    public Conference(int conferenceIdentifian, String StatutIdentifian, Calendar cal) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    //Création de méthode
    public int getIdConference() {
        return this.idConference;
    }
    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }

    public String getTitreConference() {
        return this.titreConference;
    }
    public void setTitreConference(String _titreConference) {
        this.titreConference = _titreConference;
    }
    
    public Calendar getDateConference() {
        return this.dateConference;
    }
    public void setDateConference(Calendar _dateConference) {
        this.dateConference = _dateConference;
    }

    public int getIdConferencier() {
        return this.idConferencier;
    }
    public void setIdConferencier(int _idConferencier) {
        this.idConferencier = _idConferencier;
    }

    public int getIdSalle() {
        return this.idSalle;
    }
    public void setIdSalle(int _idSalle) {
        this.idSalle = _idSalle;
    }

    public int getIdTheme() {
        return this.idTheme;
    }
    public void setIdTheme(int _idTheme) {
        this.idTheme = _idTheme;
    }
    
    public String getDateString(){
    //Classe qui permet de créer un masque de saisie pour convertir un Calendar en une chaine de caractère
    SimpleDateFormat dateConference = new SimpleDateFormat("dd/MM/yyyy");
    return dateConference.format(this.getDateConference().getTimeInMillis());
    }

    public String getNameConferencier() {
        return this.nameConferencier;
    }
    public void setNameConferencier(String _nameConferencier) {
        this.nameConferencier = _nameConferencier;
    }
    
    public java.sql.Date getSqlDate() {
        java.sql.Date sqlDate = new java.sql.Date(dateConference.getTimeInMillis());
        return sqlDate;
    }
    
     public void insert(){
        PreparedStatement ps= null;
        Connection _c = getConnexion();
        String query = "INSERT INTO conference"
                +"(titreConference, dateConference, idConferencier, idSalle, idTheme)"
                +"VALUES (?, ?, ?, ?, ?)";
        try {
            ps = _c.prepareStatement(query);
            ps.setString(1, this.getTitreConference());
            ps.setDate(2, this.getSqlDate());
            ps.setInt(3, this.getIdConferencier());
            ps.setInt(4, this.getIdSalle());
            ps.setInt(5, this.getIdTheme());
            ps.execute();
        }catch (SQLException ex){
            System.err.println(ex.getMessage());
        }
    
     }
}
