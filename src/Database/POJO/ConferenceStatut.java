/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

import java.util.Calendar;

/**
 *
 * @author Segolène
 */
public class ConferenceStatut {
    //Déclaration des atributs
    private int conferenceIdentifiant;
    private int idStatut;
    private Calendar dateStatutConference;
    
    //Création des constructeurs
    public ConferenceStatut(int _idConference, int _idStatut, Calendar _dateStatutConference){
        this.conferenceIdentifiant = _idConference;
        this.idStatut = _idStatut;
        this.dateStatutConference = _dateStatutConference;
    }
    
    //Création des méthodes
    public int getConferenceIdentifiant(){
        return this.conferenceIdentifiant;
    }
    public void setConferenceIdentifiant(int _idConference){
        this.conferenceIdentifiant = _idConference;
    }
    
    public int getIdStatut(){
        return this.idStatut;
    }
    public void setidStatut(int _idStatut){
        this.idStatut = _idStatut;
    }
    
    public Calendar getdateStatutConference(){
        return this.dateStatutConference;
    }
    public void setdateStatutConference(Calendar _dateStatutConference){
        this.dateStatutConference = _dateStatutConference;
    }
    
    
    public java.sql.Date getSqlDateStatutConference() {
        java.sql.Date sqlDate = new java.sql.Date(this.dateStatutConference.getTimeInMillis());
        return sqlDate;
    }
}
