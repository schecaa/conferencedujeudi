/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;


/**
 *
 * @author Segolène
 */
public class Conferencier {
    //Déclaration des attributs
    private int idConferencier;
    private String nomPrenomConferencier;
    private boolean conferencierInterne;
    private int idConference;
    private String blocNoteConferencier;
    
    //Création de constructeur
    public Conferencier(){
        
    }

    public Conferencier(int _idConferencier, String _nomPrenmConferencier, int _idConference){
        this.idConferencier = _idConferencier;
        this.nomPrenomConferencier = _nomPrenmConferencier;
        this.idConference = _idConference;
    }    
    
    public Conferencier(int _idConferencier, String _nomPrenmConferencier, boolean _conferencierInterne, int _idConference, String _blocNoteConferencier){
        this.idConferencier = _idConferencier;
        this.nomPrenomConferencier = _nomPrenmConferencier;
        this.conferencierInterne = _conferencierInterne;
        this.idConference = _idConference;
        this.blocNoteConferencier = _blocNoteConferencier;
    }    

    //Création de méthode
    public int getIdConferencier() {
        return this.idConferencier;
    }
    public void setIdConferencier(int _idConferencier) {
        this.idConferencier = _idConferencier;
    }
    
    public String getNomPrenomConferencier(){
        return this.nomPrenomConferencier;
    }
    public void setNomPrenomConferencier(String _nomPrenomConferencier){
        this.nomPrenomConferencier = _nomPrenomConferencier;
    }
    
    public boolean isConferencierInterne(){
        return this.conferencierInterne;
    }
    public void setConferencierInter(boolean _conferencierInterne){
        this.conferencierInterne = _conferencierInterne;
    }
    
    public int getIdConference() {
        return this.idConference;
    }
    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }
    
    public String getBlocNoteConferecier(){
        return this.blocNoteConferencier;
    }
    public void setBlocNoteConferencier(String _blocNoteConferencier){
        this.blocNoteConferencier = _blocNoteConferencier ;
    }
}
