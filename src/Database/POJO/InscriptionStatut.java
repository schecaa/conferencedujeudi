/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

import java.util.Calendar;

/**
 *
 * @author Segolène
 */
public class InscriptionStatut {
    //Déclaration des attributs
    private int idInscription;
    private int idSalarie;
    private Calendar dateStatutInscription;
    
    //Création de constructeur
    public InscriptionStatut (int _idInscription, int _idSalarie, Calendar _dateStatutInscription){
        this.idInscription = _idInscription;
        this.idSalarie = _idSalarie;
        this.dateStatutInscription = _dateStatutInscription;
    }
    
    //Création des methodes
    public int getIdInscription(){
        return this.idInscription ;
    }
    public void setIdInscription(int _idInscription){
        this.idInscription = _idInscription;
    }
    
    public int getIdSalarie(){
        return this.idSalarie;
    }
    public void setIdSalarie(int _idSalarie){
        this.idSalarie = _idSalarie;
    }
    
    public Calendar getDateStatutInscription(){
        return this.dateStatutInscription;
    }
    public void getDateStatutInscription(Calendar _dateStatutInscription){
        this.dateStatutInscription = _dateStatutInscription ;
    }
}
