/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

/**
 *
 * @author Segolène
 */
public class Salarie {
    //Déclaration des attributs
    private int idSalarie;
    private String nomPrenomSalarie;
    
    //Créations des constructeurs
    public Salarie(int _idSalarie, String _nomPrenomSalarie){
        this.idSalarie = _idSalarie;
        this.nomPrenomSalarie = _nomPrenomSalarie ;
    }
    
    //Création des méthodes
    public int getIdSalarie(){
        return this.idSalarie;
    }
    public void setIdSalarie(int _idSalarie){
        this.idSalarie = _idSalarie;
    }
    
    public String getNomPrenomSalarie(){
        return this.nomPrenomSalarie;
    }
    public void setnomPrenomSalarie(String _nomPrenomSalarie){
        this.nomPrenomSalarie = _nomPrenomSalarie;
    }
}
