/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

/**
 *
 * @author Segolène
 */
public class Salle {
    //Déclaration des atributs
    private int idSalle;
    private String nomSalle;
    private int nbPlaceSalle;
    
    //Création des constructeurs
    public Salle(int _idSalle, String _nomSalle, int _nbPlaceSalle){
        this.idSalle = _idSalle;
        this.nomSalle = _nomSalle;
        this.nbPlaceSalle = _nbPlaceSalle;
    }
    
    //Créations des méthodes
    public int getIdSalle(){
        return this.idSalle;
    }
    public void setIdSalle(int _idSalle){
        this.idSalle = _idSalle;
    }

    public String getNomSalle(){
        return this.nomSalle;
    }
    public void setNomSalle(String _nomSalle){
        this.nomSalle = _nomSalle;
    }
    
    public int getNbPlaceSalle(){
        return this.nbPlaceSalle;
    }
    public void setNbPlaceSalle(int _nbPlaceSalle){
        this.nbPlaceSalle = _nbPlaceSalle;
    }
}
