/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

/**
 *
 * @author Segolène
 */
public class Statut {
    //Déclaration des attributs
    private int idStatut ;
    private String designationStatut ;
    
    //Créations des constructeurs
    public Statut (int _idStatut, String _designationStatut){
        this.idStatut = _idStatut;
        this.designationStatut = _designationStatut;
    }
    
    //Créations des methodes
    public int getIdStatut(){
        return this.idStatut;
    }
    public void setIdStatut(int _idStatut){
        this.idStatut = _idStatut;
    }
    
    public String getDesignationStatut(){
        return this.designationStatut;
    }
    public void setDesignationStatut(String _designationStatut){
        this.designationStatut = _designationStatut;
    }
}
