/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

/**
 *
 * @author Segolène
 */
public class Theme {
    //Déclaration des attributs
    private int idTheme;
    private String designationTheme ;
    
    //Création des constructeurs
    public Theme(int _idTheme, String _designationTheme){
        this.idTheme = _idTheme;
        this.designationTheme = _designationTheme;
    }
    
    //Création des méthodes
    public int getIdTheme(){
        return this.idTheme;
    }
    public void setIdTheme(int _idTheme){
        this.idTheme = _idTheme;
    }
    
    public String getDesignationTheme(){
        return this.designationTheme;
    }
    public void setDesignationTheme(String _designationTheme){
        this.designationTheme = _designationTheme;
    }
}
