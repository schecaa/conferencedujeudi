/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Database.POJO.Conference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Segolène
 */
public class ConferenceModele extends AbstractTableModel{
    private static ArrayList<Conference> listeDesConference = new ArrayList();
    
    public ConferenceModele(ArrayList <Conference> _listeDesConference){
        super();
        this.listeDesConference = _listeDesConference;
    }
    //Déclaration de la liste
    
    //Mise en place du tableau
    private static String [] columnsName = {"Titre", "Date", "Conférencier"};
    
    @Override
    public int getRowCount() {
        return listeDesConference.size();
    }

    @Override
    public int getColumnCount() {
        return this.columnsName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
    switch (columnIndex){
        case 0:{
            return listeDesConference.get(rowIndex).getTitreConference();
        }
        case 1:{
            return listeDesConference.get(rowIndex).getDateString();
        }
        case 2:{
            return listeDesConference.get(rowIndex).getNameConferencier();
        }
    }
        return null;
    }
    
    @Override
    public String getColumnName(int index){
        return columnsName[index];
    }

}
