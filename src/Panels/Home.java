/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Panels;

import Database.DAO.ConferenceDAO;
import Models.ConferenceModele;
import Database.POJO.Conference;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author Segolène
 */
public class Home extends JPanel {
//variable de classe

    private static int nbClics = 0;
    private static JLabel quatreClics = new JLabel("") ;

    public Home() {
        super();
        JLabel label = new JLabel("Bienvenue sur l'application");
        this.add(label);

        JButton button = new JButton("CLIC &… surprise !");
        button.setSize(50, 20);
        this.add(button);
        
        this.add(quatreClics);
        
        //block pour définir l'action déclacher quand on appui sur le bouton
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nbClics += 1;
                if (nbClics >= 4) {
                quatreClics.setText("Vous avez cliqué " + nbClics + " fois sur le bouton.");
                }
            }
        });
    Calendar d = Calendar.getInstance();
//    Conference conference1 = new Conference(1, "Conférence 1", 1, d, 1, 1, "Martine");
//    Conference conference2 = new Conference(1, "Conférence 2", 1, d, 1, 1, "Martin");
    ArrayList<Conference> listeDesConference = ConferenceDAO.getAll();
//    listeDesConference.add(conference2);
//    listeDesConference.add(conference1);
    ConferenceModele conferenceModel = new ConferenceModele(listeDesConference);
    JTable table = new JTable(conferenceModel);
    table.setSize(320,400);
    JScrollPane scrollPane = new JScrollPane (table);
    scrollPane.setSize(3,400);
    this.add(scrollPane);
        
    }
}
