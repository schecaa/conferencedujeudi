package conferencedujeudi;

import Panels.Home;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Segolène
 */
public class CustomWindow extends JFrame implements ActionListener {

    private static final String TITRE = "Ma première fenetre";
    private static final int WIDTH = 1024;
    private static final int HEIGHT = 700;
    private JMenuItem fermer;

    public CustomWindow() {
        super(TITRE);
        this.setSize(WIDTH, HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //permet de fermer la fenetre et d'arreter l'application

        //Création de la barre menu
        //JMenuBar c'est la classe de menuBar
        //menuBar c'est un objet/instance de JMenuBar
        JMenuBar menuBar = new JMenuBar();
        menuBar.isVisible(); //rendre visible
        this.setJMenuBar(menuBar);

        //Création des boutons s'affichant sur la barre menu
        JMenu fichier = new JMenu("Fichier");
        menuBar.add(fichier); //affectation de boutton fichier au menu

        //Création de la liste déroulante sour le bouton fichier
        fermer = new JMenuItem("Fermer");
        fichier.add(fermer); //affecter le bouton fermer au bouton fichier
        fermer.addActionListener(this);

        //Création des boutons s'affichant sur la barre menu
        JMenu conference = new JMenu("Conférence");
        menuBar.add(conference); //affectation de boutton conférence au menu
        menuBar.setVisible(true); //rendre visible

        //Création de la liste déroulante sour le bouton Conférence
        JMenuItem voirListeConference = new JMenuItem("Voir la liste des conférences");
        conference.add(voirListeConference);

        //Création de la liste déroulante sour le bouton Conférence
        JMenuItem ajouterConference = new JMenuItem("Ajouter une conférence");
        conference.add(ajouterConference);
        
        //Création de l'instance Home
        Home accueil = new Home();
        this.setContentPane(accueil); //ajout dans la page accueil
        this.repaint();
        this.revalidate();
        
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == fermer) {
            System.exit(0);
        }
    }

}
